﻿(window.layoutPreviewers = window.layoutPreviewers || { })
    .zone = function(description, target) {
        var zone = $("<div></div>", {
                src: description.attr("src"),
                title: description.attr("title")
            })
            .addClass(description.attr("class") || "")
            .html((description.attr("title") ? description.attr("title") + ": " : "") + (description.attr("zone") || "Zone"));
        if (typeof description.attr("left") !== "undefined" || typeof description.attr("top") !== "undefined") {
            zone.css({
                position: "absolute",
                top: description.attr("top"),
                left: description.attr("left")
            });
        }
        target.append(zone);
        return zone;
    };